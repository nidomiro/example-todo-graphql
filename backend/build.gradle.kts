import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.1.7.RELEASE"
	id("io.spring.dependency-management") version "1.0.8.RELEASE"
	kotlin("jvm") version "1.3.50"
	kotlin("plugin.spring") version "1.3.50"
}

group = "de.nidomiro.example-todo-graphql"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("com.graphql-java-kickstart:graphql-java-tools:5.6.1")

	compile("com.graphql-java-kickstart:graphql-spring-boot-starter:5.10.0")
	runtime("com.graphql-java-kickstart:graphiql-spring-boot-starter:5.10.0")
	runtime("com.graphql-java-kickstart:playground-spring-boot-starter:5.10.0")


	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testCompile("com.graphql-java-kickstart:graphql-spring-boot-starter-test:5.10.0")

}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}
