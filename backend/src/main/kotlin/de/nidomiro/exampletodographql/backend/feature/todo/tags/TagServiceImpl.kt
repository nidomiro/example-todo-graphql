package de.nidomiro.exampletodographql.backend.feature.todo.tags

import de.nidomiro.exampletodographql.backend.IdGenerator
import org.springframework.stereotype.Service

@Service
class TagServiceImpl : TagService {

    private val idGenerator: Long by IdGenerator()

    private val data = mutableMapOf<Long, TagDto>()


    override fun getById(id: Long): TagDto? = data[id]

    override fun getAll(): List<TagDto> = data.values.toList()

    override fun getAllBy(todoId: Long): List<TagDto> = data.values.filter { it.todoIds.contains(todoId) }

    override fun save(dto: TagDto): TagDto {
        data[dto.id] = dto
        return dto
    }

    override fun create(inputDtoCreate: CreateTagInputDto): TagDto {
        val dto = TagDto(idGenerator, inputDtoCreate.name, inputDtoCreate.todoIds ?: setOf())
        return save(dto)
    }

    override fun remove(id: Long): TagDto? {
        return data.remove(id)
    }

    override fun getByName(tagName: String): TagDto? {
        return data.values.firstOrNull { it.name == tagName }
    }

    override fun addTodoIdToTag(tagName: String, todoId: Long) {
        getByName(tagName)?.let { tag ->
            save(tag.copy(todoIds = tag.todoIds + setOf(todoId)))
        }
            ?: create(CreateTagInputDto(tagName, setOf(todoId)))
    }

    override fun removeTodoIdFromTag(tagName: String, todoId: Long) {
        getByName(tagName)?.let { tag ->
            save(tag.copy(todoIds = tag.todoIds - setOf(todoId)))
        }
    }
}