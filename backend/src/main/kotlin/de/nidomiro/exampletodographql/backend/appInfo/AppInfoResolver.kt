package de.nidomiro.exampletodographql.backend.appInfo

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.stereotype.Service

@Service
class AppInfoResolver(
    internal val appInfoService: AppInfoService
) : GraphQLQueryResolver {

    fun appInfo() = appInfoService.appInfo

}