package de.nidomiro.exampletodographql.backend.feature.todo

import com.coxautodev.graphql.tools.GraphQLResolver
import de.nidomiro.exampletodographql.backend.feature.todo.tags.TagDto
import de.nidomiro.exampletodographql.backend.feature.todo.tags.TagService
import org.springframework.stereotype.Service

@Service
@Suppress("unused")
class TodoTypeResolver(
    private val tagService: TagService
) : GraphQLResolver<TodoDto> {

    fun tags(dto: TodoDto): List<TagDto> = tagService.getAllBy(dto.id)
}