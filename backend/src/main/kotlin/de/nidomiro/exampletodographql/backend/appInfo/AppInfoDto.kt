package de.nidomiro.exampletodographql.backend.appInfo

data class AppInfoDto(
    val apiMayorVersion: Int,
    val apiMinorVersion: Int,
    val apiVersion: String = "$apiMayorVersion.$apiMinorVersion"
)