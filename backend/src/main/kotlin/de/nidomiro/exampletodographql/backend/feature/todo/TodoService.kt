package de.nidomiro.exampletodographql.backend.feature.todo

interface TodoService {

    fun getById(id: Long): TodoDto?
    fun getAll(): List<TodoDto>
    fun getAllBy(tagName: String): List<TodoDto>?

    fun save(dto: TodoDto): TodoDto
    fun create(inputDtoCreate: CreateTodoInputDto): TodoDto
    fun remove(id: Long): TodoDto?
}