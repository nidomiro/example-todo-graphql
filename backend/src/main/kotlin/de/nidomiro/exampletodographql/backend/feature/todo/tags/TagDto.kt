package de.nidomiro.exampletodographql.backend.feature.todo.tags

data class TagDto(
    val id: Long,
    val name: String,
    val todoIds: Set<Long>
)