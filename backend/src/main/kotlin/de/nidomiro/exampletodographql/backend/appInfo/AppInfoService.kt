package de.nidomiro.exampletodographql.backend.appInfo

interface AppInfoService {
    val appInfo: AppInfoDto
}