package de.nidomiro.exampletodographql.backend.feature.todo.tags

data class AddTagToTodoInputDto(
    val todoId: Long,
    val tag: CreateTagInputDto
)