package de.nidomiro.exampletodographql.backend

import de.nidomiro.exampletodographql.backend.feature.todo.CreateTodoInputDto
import de.nidomiro.exampletodographql.backend.feature.todo.TodoService
import de.nidomiro.exampletodographql.backend.feature.todo.tags.CreateTagInputDto
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class ExampleDataCreator(
    private val todoService: TodoService
) {

    @PostConstruct
    fun createExampleData() {

        createTodo("Task A", setOf("important"))
        createTodo("Task B", setOf("important", "Tag 1"))
        createTodo("Task C", setOf("Tag 1"))
    }

    private fun createTodo(title: String, tags: Set<String>) {
        todoService.create(
            CreateTodoInputDto(
                title = title,
                tags = tags.map { CreateTagInputDto(it) }.toSet()
            )
        )
    }


}