package de.nidomiro.exampletodographql.backend

import kotlin.reflect.KProperty

class IdGenerator() {
    private var id = 0L

    operator fun getValue(thisRef: Any?, prop: KProperty<*>): Long = id++
}