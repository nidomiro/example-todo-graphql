package de.nidomiro.exampletodographql.backend.feature.todo.tags

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import de.nidomiro.exampletodographql.backend.feature.todo.TodoDto
import de.nidomiro.exampletodographql.backend.feature.todo.TodoService
import org.springframework.stereotype.Service


@Service
@Suppress("unused")
class TagResolver(
    private val tagService: TagService,
    private val todoService: TodoService
) : GraphQLQueryResolver, GraphQLMutationResolver {

    fun addTagToTodo(input: AddTagToTodoInputDto): TodoDto? {
        tagService.addTodoIdToTag(input.tag.name, input.todoId)
        return todoService.getById(input.todoId)

    }

    fun removeTagFromTodo(input: RemoveTagFromTodoInputDto): TodoDto? {
        tagService.removeTodoIdFromTag(input.tag.name, input.todoId)
        return todoService.getById(input.todoId)
    }


}