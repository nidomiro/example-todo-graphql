package de.nidomiro.exampletodographql.backend.feature.todo.tags

data class RemoveTagFromTodoInputDto(
    val todoId: Long,
    val tag: CreateTagInputDto
)