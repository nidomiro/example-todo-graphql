package de.nidomiro.exampletodographql.backend.feature.todo

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.stereotype.Service

@Service
@Suppress("unused")
class TodoResolver(private val todoService: TodoService) : GraphQLQueryResolver, GraphQLMutationResolver {

    fun getTodo(id: Long): TodoDto? = todoService.getById(id)

    fun getAllTodos() = todoService.getAll()

    fun getAllTodosByTag(tagName: String) = todoService.getAllBy(tagName)

    fun updateTodo(dto: TodoDto) = todoService.save(dto)

    fun createTodo(createDto: CreateTodoInputDto) = todoService.create(createDto)

    fun deleteTodo(id: Long) = todoService.remove(id)
}