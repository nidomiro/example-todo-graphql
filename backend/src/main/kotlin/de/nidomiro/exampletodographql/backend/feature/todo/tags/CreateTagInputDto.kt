package de.nidomiro.exampletodographql.backend.feature.todo.tags

data class CreateTagInputDto(
    val name: String,
    val todoIds: Set<Long>? = null
)