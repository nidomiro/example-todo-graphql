package de.nidomiro.exampletodographql.backend.feature.todo.tags

import org.springframework.stereotype.Service

@Service
interface TagService {

    fun getById(id: Long): TagDto?
    fun getAll(): List<TagDto>
    fun getAllBy(todoId: Long): List<TagDto>

    fun save(dto: TagDto): TagDto
    fun create(inputDtoCreate: CreateTagInputDto): TagDto
    fun remove(id: Long): TagDto?
    fun getByName(tagName: String): TagDto?

    fun addTodoIdToTag(tagName: String, todoId: Long)
    fun removeTodoIdFromTag(tagName: String, todoId: Long)


}
