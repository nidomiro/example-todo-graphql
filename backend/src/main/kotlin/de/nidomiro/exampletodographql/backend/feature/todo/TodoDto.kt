package de.nidomiro.exampletodographql.backend.feature.todo

data class TodoDto(
    val id: Long,
    val title: String
)
