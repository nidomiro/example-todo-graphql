package de.nidomiro.exampletodographql.backend.feature.todo

import de.nidomiro.exampletodographql.backend.IdGenerator
import de.nidomiro.exampletodographql.backend.feature.todo.tags.TagService
import org.springframework.stereotype.Service

@Service
class TodoServiceImpl(
    private val tagService: TagService
) : TodoService {

    private val idGenerator: Long by IdGenerator()

    private val data = mutableMapOf<Long, TodoDto>()

    override fun getById(id: Long): TodoDto? = data[id]

    override fun getAll(): List<TodoDto> = data.values.toList()

    override fun getAllBy(tagName: String): List<TodoDto>? {
        return tagService.getByName(tagName)?.let { tag ->
            data.values.filter { dto -> tag.todoIds.contains(dto.id) }
        }
    }

    override fun save(dto: TodoDto): TodoDto {
        data[dto.id] = dto
        return dto
    }

    override fun create(inputDtoCreate: CreateTodoInputDto): TodoDto {
        val id = idGenerator
        val dto = save(TodoDto(id, inputDtoCreate.title))

        inputDtoCreate.tags?.forEach { tag -> tagService.addTodoIdToTag(tag.name, id) }

        return dto
    }

    override fun remove(id: Long): TodoDto? {
        return data.remove(id)
    }
}
