package de.nidomiro.exampletodographql.backend.appInfo

import org.springframework.stereotype.Service

@Service
class AppInfoServiceImpl : AppInfoService {
    override val appInfo: AppInfoDto
        get() = AppInfoDto(1, 0)
}