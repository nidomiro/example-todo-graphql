package de.nidomiro.exampletodographql.backend.feature.todo

import de.nidomiro.exampletodographql.backend.feature.todo.tags.CreateTagInputDto

data class CreateTodoInputDto(
    val title: String,
    val tags: Set<CreateTagInputDto>? = null
)
